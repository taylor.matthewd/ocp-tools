FROM registry.access.redhat.com/ubi8:latest

LABEL oc.version="4.6"
LABEL helm.version="3.4.0"

RUN \
    dnf update -y && \
    \
    curl -O https://mirror.openshift.com/pub/openshift-v4/clients/oc/4.6/linux/oc.tar.gz && \
    tar -xvf oc.tar.gz -C /usr/local/bin/ && \
    chmod 755 /usr/local/bin/oc && \
    \
    curl -O https://get.helm.sh/helm-v3.4.0-linux-amd64.tar.gz && \
    tar -xvf helm-v3.4.0-linux-amd64.tar.gz -C /usr/local/bin/ && \
    (cd /usr/local/bin && ln -s ./linux-amd64/helm . ) && \
    \
    dnf clean all && \
    \
    rm -rf oc.tar.gz helm-v3.4.0-linux-amd64.tar.gz
