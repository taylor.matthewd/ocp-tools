# ocp-tools

## What is ocp-tools?

`ocp-tools` is a container image based on the [Red Hat Universal Base Image 8 Minimal](https://access.redhat.com/containers/#/registry.access.redhat.com/ubi8-minimal) that has utitilies added that are useful when compiling projects and interacting with OpenShift within a GitLab pipeline. It has:
* [oc](https://docs.openshift.com/container-platform/4.5/cli_reference/openshift_cli/getting-started-cli.html)
* [helm](https://helm.sh/)

## How do I use ocp-tools?

Specify it as the image to use in your pipeline by adding the following line:
```
image: quay.io/mataylor/ocp-tools:latest
```

## Available images

| Type | Image | Comments |
|------|-------|----------|
| base | quay.io/mataylor/ocp-tools:latest | Only has oc and helm installed. Used to build other images |
| golang | quay.io/mataylor/ocp-tools-golang:latest | Base + golang 1.13 |
